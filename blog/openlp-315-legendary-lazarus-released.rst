.. title: OpenLP 3.1.5 "Legendary Lazarus" Released
.. slug: 2024/11/17/openlp-315-legendary-lazarus-released
.. date: 2024-11-17 08:00:00 UTC
.. tags:
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /cover-images/openlp-315-legendary-lazarus.jpg

It has been a few months since our last release, and while we were working on the next major version, we fixed
some fairly serious bugs which we felt are big enough to warrant going back on our "no more 3.1.x releases"
statement to release these fixes to you.

Bug Fixes
---------

* Resolve extremely long delays with the web remote when there are multiple clients connected by fixing a deadlock
  issue in the Web API
* Fix media auto start behavior (second point of `bug #1302`_)
* Handle new version of the dependency websockets (14.0)
* Handle new version of the dependency werkzeug (3.1.3)

Known Issues
------------

See the `OpenLP 3.1 release blog post`_ for the list of known issues.

Note
----

Some astute observers may have noticed an unannounced version 3.1.4. Just as we released 3.1.4, a dependency changed
which caused issues with OpenLP, so we had to re-release it under a new version number that includes support for that
dependency.


.. _bug #1302: https://gitlab.com/openlp/openlp/-/issues/1302
.. _OpenLP 3.1 release blog post: /blog/2024/02/29/openlp-310-superb-seth-released
