.. title: OpenLP 3.1.2 "Exemplary Eleazar" Released
.. slug: 2024/05/19/openlp-312-exemplary-eleazar-released
.. date: 2024-05-19 14:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-312-exemplary-eleazar-released.jpg

It's been a few months, but we've been working on fixing some of the most frequently encountered bugs in version
3.1.0 and 3.1.1.

   Blessed is the man who doesn't walk in the counsel of the wicked,

   Nor stand in the way of sinners,

   Nor sit in the seat of scoffers;

   But his delight is in the law of Yahweh;

   On his law he meditates day and night.

   Psalm 1:1-2

New Features
----------------

* Add "Apply UPPERCASE globally" function to songs plugin
* Add EasyWorship Service file (.ewsx) song importer
* Add web API endpoint for the Web Remote to get the configured language
* Add web API endpoint for the Web Remote to get the configured shortcut keys

Enhancements
------------------

* Import additional planning center data 
* Better handling and notification of file permission errors
* Provide more integration for community plugins
* Make the slide height affect the size of the thumbnails generated

Bug Fixes
-----------

* Fix OpenLP being unable to detect VLC on macOS
* Stop Service File items from containing more than one audio file 
* Fix build part of version number 
* Hide live when screen setup has changed to prevent a possible crash
* Attempt to fix #1878 by checking if the service item exists first 
* Fix for not found i18n directory when using Web Remote
* Fix missing verse translations 
* Add checks to prevent multiple Linked Audio items on songs 
* Further fix #1871 by adding the Application name as early as possible 
* Fix unintentional change of the organization name by the domain name. 
* Fix missing translations

Known Issues
----------------

See the `OpenLP 3.1 release blog post </blog/2024/02/29/openlp-310-superb-seth-released>`_ for the list of
known issues.

Download
------------

Download the latest version of OpenLP from the `downloads </#downloads>`_ section on the site.
