.. title: OpenLP 3.1.6 "Reliable Reuel" Released
.. slug: 2025/01/12/openlp-316-reliable-reuel-released
.. date: 2025-01-12 14:00:00 UTC
.. tags:
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /cover-images/openlp-316-reliable-reuel-released.jpg

While work on OpenLP 4.0 is continuing, we were still able to fix some small but annoying bugs in the 3.1.x
series. This is the last version, we pinky promise!

Bug Fixes
---------

- Fix media autostart behavior regarding video slide items
- Attempt to prevent false antivirus warnings in Windows
- Fix an incorrect version number in some builds


Known Issues
------------

See the `OpenLP 3.1 release blog post </blog/2024/02/29/openlp-310-superb-seth-released>`_ for the list of known issues.
