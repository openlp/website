.. title: OpenLP 3.1.7 "Pondering Perez" Released
.. slug: 2025/02/16/openlp-317-pondering-perez-released
.. date: 2025-02-16 12:00:00 UTC
.. tags:
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /cover-images/openlp-317-pondering-perez-released.jpg

While work on OpenLP 4.0 is continuing, we were still able to fix some small but annoying bugs in the 3.1.x series.

Bug Fixes
---------

- PDF presentations that were disabled in release 3.1.6 by accident are available again.
- Fix issue 1733 by forcing VLC version 3.0.18.

Known Issues
------------
See the `OpenLP 3.1 release blog post <https://openlp.org/blog/2024/02/29/openlp-310-superb-seth-released>`_ for the list of known issues.
